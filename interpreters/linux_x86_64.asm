section .text
global _start
_start:
        mov rbx, mem

        xor eax, eax
        mov edi, 3
        mov rsi, inbuff
        mov edx, (1<<15) -1
        syscall

        mov dx, 1

        .loop:
                lodsb
                or al, al
                jz .exit
                cmp al,  '['
                jne .notleft
                        test byte[rbx], 0xFF
                        jnz .loop
                        mov ecx, 1
                        .next_loop:
                        lodsb
                        cmp al,  '['
                        jne .next_no
                        inc cx
                        jmp .next_loop
                        .next_no:
                        cmp al, ']'
                        jne .next_loop
                        loop .next_loop
                jmp .loop
                .notleft:
                cmp al, ']'
                jne .notright
                        mov ecx, 0
                        std
                        .last_loop:
                        lodsb
                        cmp al, ']'
                        jne .last_no
                        inc cx
                        jmp .last_loop
                        .last_no:
                        cmp al,  '['
                        jne .last_loop
                        loop .last_loop
                        cld
                        inc rsi
                jmp .loop
                .notright:
                cmp al, '+'
                jne .notplus
                        inc byte [rbx]
                jmp .loop
                .notplus:
                cmp al, '-'
                jne .notsub
                        dec byte [rbx]
                jmp .loop
                .notsub:
                cmp al, '<'
                jne .notpleft
                        dec rbx
                jmp .loop
                .notpleft:
                cmp al, '>'
                jne .notpright
                        inc rbx
                jmp .loop
                .notpright:
                cmp al, '.'
                jne .notout
                        push rsi
                        mov al, 1
                        mov edi, 1
                        mov rsi, rbx
                        syscall
                        pop rsi
                jmp .loop
                .notout:
                cmp al,','
                jne .loop
                        push rsi
                        xor al, al
                        xor edi, edi
                        mov rsi, rbx
                        syscall
                        pop rsi
                jmp .loop


.exit:

        mov ax, 60
        mov edi, 1
        syscall

section .bss
inbuff: resb 1<<15
mem: resb 3000