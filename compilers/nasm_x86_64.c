#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

size_t labels[256];
size_t labelname = 0;


int main(int argc, char** argv) {
        if(argc > 1) {
                int fd = open(argv[1], 0);
                if(fd < 0) {
                        perror("open");
                        return 1;
                }
                dup2(fd, 0);
        }
        int ch;
        puts("[BITS 64]");
        puts("section .bss");
        puts("mem: resb 3000");
        puts("section .text");
        puts("global _start");
        puts("_start:");
        puts("xor eax, eax");
        puts("mov rsi, mem");
        puts("mov edx, 1");

        int track = -1;
        while((ch = getchar()) != EOF) {
                switch(ch) {
                        case '+':
                                puts("inc byte [rsi]");
                        break;
                        case '-':
                                puts("dec byte [rsi]");
                        break;
                        case '>':
                                puts("inc rsi");
                        break;
                        case '<':
                                puts("dec rsi");
                        break;
                        case '[':
                                track++;
                                labels[track] = labelname++;
                                printf("L%u:\nor byte [rsi], 0\njz L%u_end\n", labels[track], labels[track]);
                        break;
                        case ']':
                                printf("jmp L%u\nL%u_end:", labels[track], labels[track]);
                                track--;
                        break;
                        case '.':
                                puts("mov ax, 1\nmov edi, 1\nsyscall");
                        break;
                        case ',':
                                puts("xor ax, ax\nmov edi, 0\nsyscall");
                        break;
                }
        }
        puts("mov ax, 60\nmov rdi, 0\nsyscall");
}
