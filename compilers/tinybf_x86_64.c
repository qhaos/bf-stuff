#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>

static size_t label_stack[256];
static char prog_buff[64];
static size_t label = 0;

int main(int argc, char** argv) {
        char* prog = prog_buff;
        int track = 0;
        if(argc > 1) {
                int fd = open(argv[1], 0);
                if(fd < 0) {
                        perror("open");
                        return 1;
                }
                dup2(fd, 0);
        }

        puts("[BITS 64]");
        puts("section .bss");
        puts("mem: resb 3000");
        puts("section .text");
        puts("global _start");
        puts("_start:");
        puts("xor eax, eax");
        puts("mov rsi, mem");
        puts("mov edx, 1");

        char* const save = prog;
        size_t len;
        while((len = read(0, prog, 64))) {
                for(size_t i = 1; i < 4; i++) {
                        uint64_t word = *(uint64_t*)prog;
                        prog += sizeof(word);
                        for(size_t j = 0; j < 21;j++, word <<=3) {
                                switch(word&07) {
                                case 0:
                                        puts("inc byte [rsi]");
                                break;
                                case 1:
                                        puts("dec byte [rsi]");
                                break;
                                case 2:
                                        puts("inc rsi");
                                break;
                                case 3:
                                        puts("dec rsi");
                                break;
                                case 4:
                                        track++;
                                        label_stack[track] = ++label;
                                        printf("L%u:\nor byte [rsi], 0\njz L%u_end\n", label_stack[track], label_stack[track]);
                                break;
                                case 5:
                                        printf("jmp L%u\nL%u_end:", label_stack[track], label_stack[track]);
                                        track--;
                                break;
                                case 6:
                                        puts("mov ax, 1\nmov edi, 1\nsyscall");
                                break;
                                case 7:
                                        puts("xor ax, ax\nmov edi, 0\nsyscall");
                                break;
                                }
                        }
                }
                prog = save;
        }


        puts("mov ax, 60\nmov rdi, 0\nsyscall");
}
