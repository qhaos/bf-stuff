#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <memory.h>
#include <sys/mman.h>
#include <unistd.h>

size_t offset_stack[0x10000];
size_t* stack_ptr = &offset_stack[0];

uint8_t lookup[0x7F][11] = {
  ['+'] = { 0xFE, 0x06 },
  ['-'] = { 0xFE, 0x0E },
  ['>'] = { 0x48, 0xFF, 0xC6 },
  ['<'] = { 0x48, 0xFF, 0xCE },
  ['.'] = { 0x66, 0xB8, 0x01, 0x00, 0xBF, 0x01, 0x00, 0x00, 0x00, 0x0F, 0x05 },
  [','] = { 0x31, 0xC0, 0x31, 0xFF, 0x0F, 0x05 },
  ['['] = { 0x80, 0x0E, 0x00, 0x0F, 0x84, 0x00, 0x00, 0x00, 0x00 },
  [']'] = { 0xE9, 0x00, 0x00, 0x00, 0x00 },
};

uint8_t size_lookup[0x7F] = {
  ['+'] = 2,
  ['-'] = 2,
  ['>'] = 3,
  ['<'] = 3,
  ['.'] = 11,
  [','] = 6,
  ['['] = 9,
  [']'] = 5,
};

static uint8_t bootstrap[] = {
  0x48, 0x81, 0xec, 0x00, 0x10, 0x00, 0x00, 0x48, 0x89, 0xe6, 0xb9, 0x00,
  0x10, 0x00, 0x00, 0xc6, 0x06, 0x00, 0x48, 0xff, 0xc6, 0xe2, 0xf8, 0x48,
  0x89, 0xe6, 0xba, 0x01, 0x00, 0x00, 0x00, 0x48, 0x31, 0xc0  
};

static uint8_t epilouge[] = {
  0xb8, 0x3c, 0x00, 0x00, 0x00, 0x48, 0x31, 0xd2, 0x0f, 0x05
};


static char code[30000];

int main(int argc, char* argv[]) {
  FILE* in;
  FILE* out;

  if(argc > 1) {
    in = fopen(argv[1], "r");
    if(!in) {
      perror("-fopen");
      exit(1);
    }
  } else {
    in = stdin;
  }


  char* fname = (argc > 2)? argv[2] : "a.out";
  out = fopen(fname, "w");

  if(!out) {
    perror("fopen");
    exit(1);
  }

  size_t len = strlen(fgets(code, 0x10000, in));


  fwrite((void*) bootstrap, sizeof(bootstrap), 1, out);

  for(size_t i = 0; i < len; i++) {
    int ch = code[i];

    uint8_t* instruction = lookup[ch];

    if(!instruction)
      continue;

    if(ch == '[') {

      int j = i + 1;
      int count = 1;
      size_t offset = 0;
      while(count) {
        if(j >= len) {
          fputs("ERROR: loop kept looping\n",stderr);
          return 1;
        }
        offset += size_lookup[code[j]];
        count += (code[j] == '[') - (code[j] == ']');
        j++;
      }

      *(stack_ptr++) = -offset;

      *(uint32_t*)(instruction+5) = offset;
    } else if(ch == ']') {

      if(stack_ptr == &offset_stack[0]) {
        fputs("stack underflow! double check that your [ match your ]\n", stderr);
      }

      *(int32_t*)(instruction+1) = (*(--stack_ptr)) - 9;
    }

    fwrite(instruction, size_lookup[ch], 1, out);
  }

  fwrite((void*) epilouge, sizeof(epilouge), 1, out);
  long sz = ftell(out);
  uint8_t* prog;
  if(posix_memalign((void**) &prog, sysconf(_SC_PAGESIZE), sz) || prog == NULL) {
    perror("posix_memalign");
    exit(1);
  }

  fclose(out);

  out = fopen(fname, "r");


  /* read the program into memory, set that memory as executable, and run it */
  if(!fread(prog, sz, 1, out)) {
    perror("fread");
    exit(1);
  }

  if(mprotect(prog, sz, PROT_EXEC | PROT_READ) < 0) {
    perror("mprotect");
    exit(1);
  }

  __asm__("call *%%rax" : : "a"(prog));

  /* shouldn't reach here */
  return 1;
}
